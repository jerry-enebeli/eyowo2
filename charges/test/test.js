var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
var expect = chai.expect;

const { transfers } = require('../app/controllers/fundstransferctrl')

describe('transfer to wallet', () => {
  it('should return a success message', () => {
    expect(transfers()).to.eventually.eqls('ss');
  });
});
