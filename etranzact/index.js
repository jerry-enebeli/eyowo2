const grpc = require('grpc');
const redis = require('redis');
const rmdstr = require('randomstring');
const jwt = require('jsonwebtoken');
// const conn = require('./config/db');
// const sms = require('./util/sms');
// const redisUtil = require('./util/redis.js');
const uuid = require('node-uuid');
const crypto = require('crypto');
const request = require('./util/request');
const mongoose = require('mongoose');
const soap = require('soap');
const transformXMLtoJSON = require('camaro');


require('dotenv').config();


const PROTO_PATH = `${__dirname}/proto/etranzact.proto`;
const eTranzactService = grpc.load(PROTO_PATH).eTranzact;

// const User = conn.models.user;

// const redisClient = redis.createClient(process.env.REDIS_HOST); // creates a new client

/*
redisClient.on('connect', () => {
  console.log('Connected to Redis!');
}); */

const liveURL = 'https://www.etranzact.net/SwitchIT/doc.wsdl';
// const switchITUrl = 'https://www.etranzact.net/SwitchIT/doc.wsdl';
const bankITURL = 'https://www.etranzact.net/bankIT/service_bankitapi?json=';

const etranzactPaymentLiveObj = {
  clientid: '7007139203',
  terminalid: '7007139203',
  merchantcode: '700602X2HZ',
  clientkey: 'cLMTqHYLKmP5fChCCR3atPbfbnzFAqYY',
};

const bankITStatus = {
  0: 'Transaction Successful',
  1: 'Destination Card Not Found, 2 - Card Number Not Found',
  3: 'Invalid Card PIN',
  4: 'Card Expiration Incorrect',
  5: 'Insufficient balance',
  6: 'Spending Limit Exceeded',
  7: 'Internal System Error Occurred, please contact the service provider',
  8: 'Financial Institution cannot authorize transaction, Please try later',
  9: 'PIN tries Exceeded',
  10: 'Card has been locked',
  11: 'Invalid Terminal Id',
  12: 'Payment Timeout',
  13: 'Destination card has been locked, 14 - Card has expired',
  15: 'PIN change required',
  16: 'Invalid Amount',
  17: 'Card has been disabled',
  18: 'Unable to credit this account immediately, credit will be done later',
  19: 'Transaction not permitted on terminal',
  20: 'Exceeds withdrawal frequency',
  21: 'Destination Card has expired',
  22: 'Destination Card Disabled',
  23: 'Source Card Disabled',
  24: 'Invalid Bank Account',
  25: 'Insufficient Balance',
  1002: 'CHECKSUM/FINAL_CHECKSUM error 100 - Duplicate session id',
  200: 'Invalid client id',
  300: 'Invalid mac',
  400: 'Expired session',
  500: 'You have entered an account number that is not tied to your phone number with bank. Pls contact your bank for assistance.',
  600: 'Invalid account id',
  700: 'Security violation Please contact support@etranzact.com',
  800: 'Invalid esa code',
  801: 'Phone number and account number not matched',
  900: 'Transaction limit exceeded',
  Z: 'Pending authorization',
};

async function balanceEnquiry(call, callback) {
  const requestArgs = {
    request: {
      direction: 'request',
      action: 'BE',
      terminalId: '7017010007',
      transaction: {
        pin: 'GVIXZ19oLpzUbRMHjaQ5Hw==',
        reference: mongoose.Types.ObjectId().toString(),
      },
    },
  };
  soap.createClient(liveURL, (err, client) => {
    if (err) {
      console.error(err);
      callback(err);
      return;
    }
    client.process(requestArgs, (errr, result) => {
      if (errr) {
        console.error(errr);
        callback(errr);
        return;
      }
      console.log(result);
      callback(null, { success: true, data: result, status: 200 });
    });
  });
}
async function queryBillers(call, callback) {
  const requestArgs = {
    request: {
      direction: 'request',
      action: 'BI',
      terminalId: '7017010007',
      transaction: {
        pin: 'GVIXZ19oLpzUbRMHjaQ5Hw==',
        reference: mongoose.Types.ObjectId().toString(),
      },
    },
  };
  soap.createClient(liveURL, (err, client) => {
    if (err) {
      console.error(err);
      callback(err);
      return;
    }
    client.process(requestArgs, (errr, result) => {
      if (errr) {
        console.error(errr);
        callback(errr);
        return;
      }
      console.log(result);
      const template = {
        billers: ['//biller', {
          code: 'billerMerchantCode',
          alias: 'billerAlias',
          description: 'billerDescr',
        }],
      };

      const resultJSON = transformXMLtoJSON(result.response.message, template);
      callback(null, { success: true, data: resultJSON, status: 200 });
    });
  });
}
async function queryAccountInfo(call, callback) {
  const { bankCode } = call.request;
  const { accountNumber } = call.request;

  const requestArgs = {
    request: {
      direction: 'request',
      action: 'AQ',
      terminalId: '7017010007',
      transaction: {
        pin: 'GVIXZ19oLpzUbRMHjaQ5Hw==',
        reference: mongoose.Types.ObjectId().toString(),
        bankCode,
        endPoint: 'A',
        destination: accountNumber,
      },
    },
  };
  soap.createClient(liveURL, (err, client) => {
    if (err) {
      console.error(err);
      callback(err);
      return;
    }
    client.process(requestArgs, (errr, result) => {
      if (errr) {
        console.error(errr);
        callback(errr);
        return;
      }
      console.log(result);
      if (result.response.error === '0') {
        callback({ success: true, data: result.response.message, status: 200 });
        return;
      }
      callback({ success: false, err: 'Account not found', status: 400 });
    });
  });
}
async function getDSTVBouquet(call, callback) {
  const requestArgs = {
    request: {
      direction: 'request',
      action: 'PB',
      terminalId: '7000000001',
      transaction: {
        id: '1',
        pin: 'kghxqwveJ3eSQJip/cmaMQ==',
        reference: mongoose.Types.ObjectId().toString(),
        lineType: 'DSTV',
        destination: '41157294764',
        senderName: 'eTrazact test',
        description: 'DSTV Payment',
      },
    },
  };

  soap.createClient(liveURL, (err, client) => {
    if (err) {
      console.error(err);
      callback(err);
      return;
    }
    client.process(requestArgs, (errr, result) => {
      if (errr) {
        console.error(errr);
        callback(errr);
        return;
      }
      console.log(result);
      const template = {
        items: ['//Product', {
          name: 'ProductName',
          amount: 'ProductAmount',
        }],
      };
      const resultJSON = transformXMLtoJSON(result.response.message, template);
      callback({ success: true, data: resultJSON, status: 200 });
    });
  });
}
function getBanks(call, callback) {
  console.log('HERE');
  const requestArgs = {
    request: {
      direction: 'request',
      action: 'BL',
      terminalId: '7017010007',
      transaction: {
        pin: 'GVIXZ19oLpzUbRMHjaQ5Hw==',
        reference: mongoose.Types.ObjectId().toString(),
      },
    },
  };

  soap.createClient(liveURL, (err, client) => {
    if (err) {
      console.error(err);
      callback(err);
      return;
    }
    client.process(requestArgs, (errr, result) => {
      if (errr) {
        console.error(errr);
        callback(errr);
        return;
      }
      console.log(result);
      const template = {
        banks: ['//bank', {
          code: 'bankCode',
          name: 'bankName',
          alias: 'bankAlias',
        }],
      };
      const resultJSON = transformXMLtoJSON(result.response.message, template);
      callback(null, { success: true, data: JSON.stringify(resultJSON), status: 200 });
    });
  });
}

async function depositInitiate(call, callback) {
  const { id } = call.request;
  const { amount } = call.request;
  const { account_number } = call.request;
  const { description } = call.request;
  const { bank } = call.request;

  try {
    const user = await User.findOne({ _id: id }).exec();
    if (user) {
      const paymentObj = {
        clientid: etranzactPaymentLiveObj.clientid,
        terminalid: etranzactPaymentLiveObj.terminalid,
        merchantcode: etranzactPaymentLiveObj.merchantcode,
        action: 'initialize',
        sessionid: uuid.v4().toString(),
        transactionid: uuid.v4().toString(),
        bank,
        accountnumber: account_number,
        amount,
        description,
        serviceid: 'AC76374',
      };

      // amountCache('store', paymentObj.sessionid, paymentObj.amount);

      const macString = paymentObj.sessionid + paymentObj.clientid + paymentObj.terminalid + paymentObj.transactionid + paymentObj.accountnumber + paymentObj.bank + paymentObj.serviceid + paymentObj.amount + paymentObj.description + paymentObj.merchantcode + etranzactPaymentLiveObj.clientkey;
      const mac = crypto.createHash('sha256').update(macString).digest('hex');
      paymentObj.mac = mac;
      console.log(paymentObj.mac);

      const finalURL = bankITURL + JSON.stringify(paymentObj);

      console.log(finalURL);

      const requestOptions = {
        method: 'POST',
        uri: finalURL,
        json: true, // Automatically stringifies the body to JSON
      };

      const initializeRequest = await request.postToURL(requestOptions);
      console.log(initializeRequest);
      if (initializeRequest.accounts === undefined) {
        callback(null, {
          success: true, freshAccount: true, data: initializeRequest, status: 200,
        });
        return;
      }
      callback(null, {
        success: true, freshAccount: false, data: initializeRequest, status: 200,
      });
      return;
    }
  } catch (err) {
    console.error(err);
    callback(err);
  }
}
async function depositSubmitAccountDetails(call, callback) {
  const { id } = call.request;
  const { freshAccount } = call.request;
  const { requestid } = call.request;
  const { sessionid } = call.request;
  const { fname } = call.request;
  const { lname } = call.request;
  const { email } = call.request;
  const { passcode } = call.request;
  const { accountid } = call.request;
  try {
    if (freshAccount) {
      const newAccountObj = {
        clientid: etranzactPaymentLiveObj.clientid,
        requestid,
        action: 'newaccountinfo',
        sessionid,
        firstname: fname,
        lastname: lname,
        emailaddress: email,
        passcode,
      };

      const macString2 = newAccountObj.sessionid + newAccountObj.clientid + newAccountObj.firstname + newAccountObj.lastname + newAccountObj.emailaddress + newAccountObj.passcode + etranzactPaymentLiveObj.clientkey;
      const mac2 = crypto.createHash('sha256').update(macString2).digest('hex');

      newAccountObj.mac = mac2;
      console.log(newAccountObj.mac);

      const finalURL2 = bankITURL + JSON.stringify(newAccountObj);
      console.log(finalURL2);

      const requestOptions = {
        method: 'POST',
        uri: finalURL2,
        json: true, // Automatically stringifies the body to JSON
      };

      const initializeRequest = await request.postToURL(requestOptions);
      console.log(initializeRequest);
      callback(null, {
        success: true, data: initializeRequest, status: 200,
      });
      return;
    }
    const newAccountObj = {
      clientid: etranzactPaymentLiveObj.clientid,
      requestid,
      action: 'accountinfo',
      sessionid,
      accountid,
      passcode,
    };

    const macString2 = newAccountObj.sessionid + newAccountObj.clientid + newAccountObj.accountid + newAccountObj.passcode + etranzactPaymentLiveObj.clientkey;
    const mac2 = crypto.createHash('sha256').update(macString2).digest('hex');

    newAccountObj.mac = mac2;
    console.log(newAccountObj.mac);

    const finalURL2 = bankITURL + JSON.stringify(newAccountObj);
    console.log(finalURL2);

    const requestOptions = {
      method: 'POST',
      uri: finalURL2,
      json: true, // Automatically stringifies the body to JSON
    };

    const initializeRequest = await request.postToURL(requestOptions);
    console.log(initializeRequest);
    callback(null, {
      success: true, data: initializeRequest, status: 200,
    });
    return;
  } catch (err) {
    console.error(err);
    callback(err);
  }
}

async function depostFinalize(call, callback) {
  const { id } = call.request;
  const { requestid } = call.request;
  const { sessionid } = call.request;
  const { esacode } = call.request;

  const newAccountObj = {
    clientid: etranzactPaymentLiveObj.clientid,
    requestid,
    action: 'makepayment',
    sessionid,
    esacode,
  };

  const macString2 = newAccountObj.sessionid + newAccountObj.clientid + newAccountObj.esacode + etranzactPaymentLiveObj.clientkey;
  const mac2 = crypto.createHash('sha256').update(macString2).digest('hex');

  newAccountObj.mac = mac2;
  console.log(newAccountObj.mac);

  const finalURL2 = bankITURL + JSON.stringify(newAccountObj);
  console.log(finalURL2);

  const requestOptions = {
    method: 'POST',
    uri: finalURL2,
    json: true, // Automatically stringifies the body to JSON
  };

  const response = await request.postToURL(requestOptions);
  console.log(response);
  if (response.status === '0') {
    console.log(id);
    // const amount = parseInt(amountCache('retrieve', newAccountObj.sessionid), 10);
    redisClient.get(redisUtil.createRedisKey(`AMOUNT_CACHE%${response.sessionid}`), (err, amount) => {
      if (err) {
        console.error(err);
      }
      console.log(amount);
      console.log(redisUtil.createRedisKey(`AMOUNT_CACHE%${response.sessionid}`));
      redisClient.del(redisUtil.createRedisKey(`AMOUNT_CACHE%${response.sessionid}`));
      User.update({ _id: id }, { $inc: { wallet: parseInt(amount, 10) } }, (errr, numAffected) => {
        if (errr) {
          console.error(errr);
        }
        console.log(numAffected);
      });
      fundsTransferCtrl.addTransaction(userId, response, amount, 'Deposit');
    });
    return { success: true, data: response, status: 200 };
  }
  return {
    success: false, data: response, status: 400, reason: bankITStatus[response.status],
  };
}


function main() {
  const server = new grpc.Server();
  server.addService(
    eTranzactService.eTranzact.service,
    {
      balanceEnquiry,
      queryBillers,
      queryAccountInfo,
      getDSTVBouquet,
      getBanks,
      depositInitiate,
      depostFinalize,
      depositSubmitAccountDetails,
    },
  );
  server.bind('0.0.0.0:50055', grpc.ServerCredentials.createInsecure());
  server.start();

  console.log('Eyowo eTranzact Service Started!!!');
}

main();
