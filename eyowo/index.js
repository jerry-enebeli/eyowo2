const grpc = require('grpc');
const redis = require('redis');
const Rabbit = require('./util/rabbit');
const sms = require('./util/message');
const Charge = require('./charge/charge');
const hash = require('./util/crypto');
const dbConnection = require('./config/db');

const PROTO_PATH = `${__dirname}/proto/user.proto`;
const UserService = grpc.load(PROTO_PATH).eyowo_user;

const User = require('./models/user');

// const redisClient = redis.createClient(process.env.REDIS_HOST); // creates a new client

// redisClient.on('connect', () => {
//   console.log('Connected to Redis!');
// });

dbConnection();

function addTransaction(mobile, amount, remark, type) {
  switch (type) {
    case 'credit':
      Rabbit.publish(
        'amqps://eyowo:eyowo@portal1464-27.eyowo.3574791653.composedb.com:28897/eyowo',
        'transaction-add',
        JSON.stringify({
          type: 'received',
          amount,
          user: mobile,
          date: new Date(),
          remark,
        }),
      );
      break;
    case 'debit':
      Rabbit.publish(
        'amqps://eyowo:eyowo@portal1464-27.eyowo.3574791653.composedb.com:28897/eyowo',
        'transaction-add',
        JSON.stringify({
          type: 'transferred',
          amount,
          user: mobile,
          date: new Date().getTime(),
          remark,
        }),
      );
      break;
    default:
      console.log('not a type');
      break;
  }
}

const Credit = async (mobile, amount) => {
  try {
    const numAffected = await User.update(
      { mobile },
      { $inc: { wallet: 0 + amount } },
    );

    if (numAffected.n > 0) {
      sms.sms(JSON.stringify({
        mobile,
        message: `Your Eyowo Wallet Has Been Credited with N${amount} On ${new Date().toLocaleString()}`,
        sender: 'Eyowo',
      }));
      addTransaction(mobile, amount, 'test', 'credit');
      return { success: true, data: 'user credited' };
    }
    return { success: false, data: 'user not credited' };
  } catch (err) {
    console.log(err);
  }
};

const BulkCredit = async (sender, data) => {
  try {
    let NumOfSuccess = 0;

    data.forEach(async (element) => {
      const Creditdata = await Credit(element.wallet, element.amount);

      if (Creditdata.success === true) {
        NumOfSuccess += 1;
        addTransaction(element.wallet, element.amount, `${sender}bulk`);
      } else {
        // add to transaction history
        console.log(element);
      }
      if (data.length === NumOfSuccess) {
        return { success: true, data: 'users credited' };
      }
    });
  } catch (err) {
    console.log('buil-credit', err);
  }
};

const Debit = async (mobile, amount) => {
  try {
    const numAffected = await User.update(
      { mobile, wallet: { $gte: amount } },
      { $inc: { wallet: 0 - amount } },
    );
    if (numAffected.n > 0) {
      sms.sms(JSON.stringify({
        mobile,
        message: `Your Eyowo Wallet Has Been Debited with N${amount} On ${new Date().toLocaleString()}`,
        sender: 'Eyowo',
      }));
      addTransaction(mobile, amount, 'test', 'debit');
      return { success: true, data: 'user debited' };
    }
    return { success: false, data: 'user not debited' };
  } catch (err) {
    console.log(err);
  }
};

const CreateWallet = async (mobile, amount) => {
  const user = new User();
  user.mobile = mobile;
  user.wallet = amount;
  user.eyowo_pin = hash.saltHashPassword(mobile, mobile).substr(0, 7).toUpperCase();
  return new Promise((resolve, reject) => {
    user.save((err) => {
      if (err === null) {
        resolve({ success: true });
      } else {
        reject(err);
      }
    });
  });
};


const processTransferEyowo = async (senderPhone, recipientPhone, amount, securePin, remark = '', chargeCode) => {
  let status;
  // auth
  try {
    if (!securePin) {
      return { success: false, data: 'Invalid Eyowo Secure PIN' };
    }

    const user = await User.findOne({ mobile: senderPhone });

    if (!user) {
      return { success: false, data: 'Invalid Eyowo Wallet' };
    }

    if (user.compareSecurePin(securePin) === false) {
      return { success: false, data: 'Invalid Eyowo Secure PIN' };
    }

    const trancData = {
      amount,
      recipient: recipientPhone,
      sender: senderPhone,
    };

    const debitCharge = await Charge.GenerateCharge(chargeCode, 'debit', trancData);
    console.log(debitCharge.wallet, debitCharge.amount);
    status = await Debit(debitCharge.wallet, debitCharge.amount);
    if (status.success === true) {
      // credit wallet
      const creditCharge = await Charge.GenerateCharge(chargeCode, 'credit', trancData);

      if (creditCharge.length > 1) {
        const bulk = await BulkCredit(senderPhone, creditCharge);
      } else {
        console.log(creditCharge);
        status = await Credit(creditCharge[0].wallet, creditCharge[0].amount);
        console.log(status);
      }

      if (status.success === true) {
        return { success: true, data: 'fund delivered' };
      }
      const UserStatus = await CreateWallet(recipientPhone, amount);
      console.log(UserStatus);
      if (UserStatus.success === true) {
        sms.sms(JSON.stringify({
          mobile: recipientPhone,
          message: `Your Eyowo Wallet Has Been Credited with N${amount} On ${new Date().toLocaleString()}`,
          sender: 'Eyowo',
        }));
        return { success: true, data: 'fund delivered' };
      }
    } else {
      return { success: false, data: 'Insufficient Fund' };
    }
  } catch (err) {
    console.error(err);
    // We couldn't process funds, return the sender's funds
    User.update(
      { mobile: senderPhone },
      { $inc: { wallet: 0 + amount } },
      (errrrrrrr) => {
        if (errrrrrrr) {
          console.log(`Funds reversal error due to error: ${errrrrrrr}. Please fix manually`);
        }
      },
    );
    return { success: false, data: JSON.stringify(err) };
  }
};

const creditUserWithMobile = async (call, callback) => {
  try {
    const req = call.request;
    console.log(req)
    const response = await processTransferEyowo(
      req.sender,
      req.recipient,
      req.amount,
      req.pin,
      req.remark,
      req.chargeCode,
    );
    if (response.success) {
      return callback(null, response);
    }
    return callback(null, response);
  } catch (errr) {
    return 'END Funds transfer failed.';
  }
};

const creditUserWithCard = async (call, callback) => {
  const req = call.request;
  const { amount, card, mobile } = req;

  // credit user
  Credit(mobile, amount);

  const user = await User.update(mobile, { $push: JSON.parse(card) });

  if (user.n > 0) {
    callback(null, ({ success: true, data: 'card added' }));
  }
  callback(null, ({ success: false, data: 'card not added' }));
};

async function getUserWalletBalanceWithMobile(call, callback) {
  const { mobile } = call.request;
  console.log(mobile);
  try {
    const user = await User.findOne({ mobile }).exec();

    if (user) {
      console.log(user);
      callback(null, { success: true, data: String(user.wallet), status: 200 });
      return;
    }
    callback(null, { success: false, data: 'User not found', status: 400 });
    return;
  } catch (err) {
    console.error(err);
    callback(err);
  }
}

async function getUserWalletBalanceWithID(call, callback) {
  const { id } = call.request;
  try {
    const user = await User.findOne({ _id: id }).exec();
    if (user) {
      callback(null, { success: true, balance: user.wallet, status: 200 });
      return;
    }
    callback(null, { success: false, err: 'User not found', status: 400 });
    return;
  } catch (err) {
    console.error(err);
    callback(err);
  }
}

async function userExistsWithMobile(call, callback) {
  const { mobile } = call.request;
  try {
    const user = await User.findOne({ mobile }).exec();
    if (user) {
      callback(null, { success: true, status: 200 });
      return;
    }
    callback(null, { success: false, err: 'User not found', status: 400 });
    return;
  } catch (err) {
    console.error(err);
    callback(err);
  }
}

async function getUserDetails(call, callback) {
  const { mobile } = call.request;
  try {
    const user = await User.findOne({ mobile }).exec();
    if (user) {
      callback(null, { success: true, status: 200, user: JSON.stringify(user) });
      return;
    }
    callback(null, { success: false, err: 'User not found', status: 400 });
    return;
  } catch (err) {
    console.error(err);
    callback(err);
  }
}


async function addRecoveryNumber(call, callback) {
  try {
    const req = call.request;
    const user = await User.update({ _id: req.id }, { recovery: req.mobile });
    if (user.n > 0) {
      callback(null, ({ success: true, status: 200, data: 'recovery number added' }));
    }
    callback(null, ({ success: true, status: 400, data: 'number not added' }));
  } catch (err) {
    console.error(err);
    callback(err);
  }
}

const recoverPassword = async (call, callback) => {
  const req = call.request;
  try {
    const user = await User.findOne({ _id: req.id });
    if (user) {
      if (user.recovery !== req.recovery) {
        return callback(null, ({ success: false, status: 400, data: 'invalid recovery number' }));
      }
      const newCode = Math.floor(100000 + Math.random() * 900000);
    }
  } catch (error) {
    console.log(error);
  }
}

async function userExistsWithID(call, callback) {
  const { id } = call.request;
  try {
    const user = await User.findOne({ _id: id }).exec();
    if (user) {
      callback(null, { success: true, status: 200 });
      return;
    }
    callback(null, { success: false, err: 'User not found', status: 400 });
    return;
  } catch (err) {
    console.error(err);
    callback(err);
  }
}

async function getUserBankAccountBeneficiariesWithID(call, callback) {
  const { id } = call.request;
  try {
    const user = await User.findOne({ _id: id }).exec();
    if (user) {
      // console.log(JSON.stringify(user.beneficiaries));
      callback(null, { success: true, beneficiaries: JSON.stringify(user.beneficiaries), status: 200 });
      return;
    }
    callback(null, { success: false, err: 'User not found', status: 400 });
    return;
  } catch (err) {
    console.error(err);
    callback(err);
  }
}

async function getUserBankAccountBeneficiariesWithMobile(call, callback) {
  const { mobile } = call.request;
  try {
    const user = await User.findOne({ mobile }).exec();
    if (user) {
      // console.log(JSON.stringify(user.beneficiaries));
      callback(null, { success: true, beneficiaries: JSON.stringify(user.beneficiaries), status: 200 });
      return;
    }
    callback(null, { success: false, err: 'User not found', status: 400 });
    return;
  } catch (err) {
    console.error(err);
    callback(err);
  }
}


function main() {
  const server = new grpc.Server();
  server.addService(
    UserService.User.service,
    {
      getUserWalletBalanceWithID,
      getUserWalletBalanceWithMobile,
      userExistsWithID,
      userExistsWithMobile,
      getUserBankAccountBeneficiariesWithMobile,
      getUserBankAccountBeneficiariesWithID,
      creditUserWithCard,
      creditUserWithMobile,
      getUserDetails,
    },
  );
  server.bind('0.0.0.0:50056', grpc.ServerCredentials.createInsecure());
  server.start();

  console.log('Eyowo Main Service Started!!!');
}

main();
