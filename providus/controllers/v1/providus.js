const soap = require('soap');

const url = 'http://154.113.16.142:9999/Payments/api?wsdl';
const mongoose = require('mongoose');

const NIPFundTransfer = (call, callback) => {
  const req = call.request;
  const args = {
    amount: req.amount,
    currency: 'NGN',
    narration: req.narration,
    transaction_reference: String(mongoose.Types.ObjectId()),
    recipient_account_number: req.recipient_account,
    recipient_bank_code: req.recipient_bank_code,
    account_name: req.account_name,
    originator_name: req.originator_name,
    username: 'test',
    password: 'test',
  };
  soap.createClient(url, (err, client) => {
    client.NIPFundTransfer(args, (err, resp) => {
      if (err) {
        callback(
          null, ({
            success: false,
            data: JSON.stringify(err),
          }),
        );
      }
      callback(
        null, ({
          success: true,
          data: JSON.stringify(resp),
        }),
      );
    });
  });
};

const GetNIPBanks = (call, callback) => {
  soap.createClient(url, (err, client) => {
    client.GetNIPBanks((err, resp) => {
      if (err) {
        callback(
          null, ({
            success: false,
            data: JSON.stringify(err),
          }),
        );
      }
      callback(
        null, ({
          success: true,
          data: JSON.stringify(resp),
        }),
      );
    });
  });
};

const GetNIPAccount = (call, callback) => {
  const { account } = call.request;
  const { bank_code } = call.request;
  const args = {
    account_number: req.account,
    bank_code: req.bank_code,
    username: 'test',
    password: 'test',
  };
  soap.createClient(url, (err, client) => {
    client.GetNIPAccount(args, (err, resp) => {
      if (err) {
        callback(
          null, ({
            success: false,
            data: JSON.stringify(err),
          }),
        );
      }
      callback(
        null, ({
          success: true,
          data: JSON.stringify(resp),
        }),
      );
    });
  });
};

const GetNIPTransactionStatus = (call, callback) => {
  const req = call.request;
  const args = {
    transaction_reference: req.reference,
  };
  soap.createClient(url, (err, client) => {
    client.GetNIPTransactionStatus(args, (err, resp) => {
      if (err) {
        callback(
          null, ({
            success: false,
            data: JSON.stringify(err),
          }),
        );
      }
      callback(
        null, ({
          success: true,
          data: JSON.stringify(resp),
        }),
      );
    });
  });
};

const GetProvidusAccount = (call, callback) => {
  const req = call.request;
  const args = {
    account_number: req.account,
    username: 'test',
    password: 'test',
  };
  soap.createClient(url, (err, client) => {
    client.GetProvidusAccount(args, (err, resp) => {
      if (err) {
        callback(
          null, ({
            success: false,
            data: JSON.stringify(err),
          }),
        );
      }
      callback(
        null, ({
          success: true,
          data: JSON.stringify(resp),
        }),
      );
    });
  });
};

const GetBVNDetails = (call, callback) => {
  const req = call.request;
  const bvn = {
    bvn: req.bvn,
    username: 'test',
    password: 'test',
  };
  soap.createClient(url, (err, client) => {
    client.GetBVNDetails(bvn, (err, resp) => {
      if (err) {
        callback(
          null, ({
            success: false,
            data: JSON.stringify(err),
          }),
        );
      }
      callback(
        null, ({
          success: true,
          data: JSON.stringify(resp),
        }),
      );
    });
  });
};

const GetProvidusTransactionStatus = (call, callback) => {
  const req = call.request;
  const args = {
    transaction_reference: req.reference,
  };
  soap.createClient(url, (err, client) => {
    client.GetProvidusTransactionStatus(args, (err, resp) => {
      if (err) {
        callback(
          null, ({
            success: false,
            data: JSON.stringify(err),
          }),
        );
      }
      callback(
        null, ({
          success: true,
          data: JSON.stringify(resp),
        }),
      );
    });
  });
};

const EnrollCustomer = (call, callback) => {
  const req = call.request;
  const args = {
    account_number: req.account,
    username: 'test',
    password: 'test',
  };
  soap.createClient(url, (err, client) => {
    client.EnrollCustomer(args, (err, resp) => {
      if (err) {
        callback(
          null, ({
            success: false,
            data: JSON.stringify(err),
          }),
        );
      }
      callback(
        null, ({
          success: true,
          data: JSON.stringify(resp),
        }),
      );
    });
  });
};

const ResendCustomerOTP = (call, callback) => {
  const req = call.request;
  const args = {
    account_number: req.account,
    username: 'test',
    password: 'test',
  };
  soap.createClient(url, (err, client) => {
    client.ResendCustomerOTP(args, (err, resp) => {
      if (err) {
        callback(
          null, ({
            success: false,
            data: JSON.stringify(err),
          }),
        );
      }
      callback(
        null, ({
          success: true,
          data: JSON.stringify(resp),
        }),
      );
    });
  });
};

const ValidateCustomer = (call, callback) => {
  const req = call.request;
  const args = {
    account_number: req.account,
    otp: req.otp,
    username: 'test',
    password: 'test',
  };
  soap.createClient(url, (err, client) => {
    if (err) throw err;
    client.ValidateCustomer(args, (err, resp) => {
      if (err) {
        callback(
          null, ({
            success: false,
            data: JSON.stringify(err),
          }),
        );
      }
      callback(
        null, ({
          success: true,
          data: JSON.stringify(resp),
        }),
      );
    });
  });
};

const ProvidusFundTransfer = (call, callback) => {
  const req = call.request;
  const args = {
    amount: req.amount,
    currency: 'NGN',
    narration: req.narration,
    transaction_reference: String(mongoose.Types.ObjectId()),
    credit_account: req.credit_account,
    debit_account: req.debit_account,
    username: 'test',
    password: 'test',
  };
  soap.createClient(url, (err, client) => {
    client.ProvidusFundTransfer(args, (err, resp) => {
      if (err) {
        callback(
          null, ({
            success: false,
            data: JSON.stringify(err),
          }),
        );
      }
      callback(
        null, ({
          success: true,
          data: JSON.stringify(resp),
        }),
      );
    });
  });
};

module.exports = {

  NIPFundTransfer,
  GetNIPBanks,
  GetNIPAccount,
  GetNIPTransactionStatus,
  GetProvidusAccount,
  GetBVNDetails,
  GetProvidusTransactionStatus,
  EnrollCustomer,
  ResendCustomerOTP,
  ValidateCustomer,
  ProvidusFundTransfer,
};
