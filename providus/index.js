const grpc = require('grpc');
const paymentCtrl = require('./controllers/v1/providus');

const PROTO_PATH = `${__dirname }/proto/providus.proto`;
const ProvidusProto = grpc.load(PROTO_PATH).payment;

const server = new grpc.Server();

server.addService(ProvidusProto.Providus.service, paymentCtrl);
server.bind('0.0.0.0:50054', grpc.ServerCredentials.createInsecure());
server.start();
console.log({ status: 'RUNNING....', service: 'EYOWO PROVIDUS' });
