const express = require('express');
const grpc = require('grpc');
const bodyParser = require('body-parser');
const Joi = require('joi');
const middlewareAuth = require('../../../middleware/auth');


const PROTO_PATH = `${__dirname}/proto/user.proto`;
const providusPROTOPATH = `${__dirname}/proto/providus.proto`;

const UserService = grpc.load(PROTO_PATH).eyowo_user;
const UserRPC = new UserService.User('0.0.0.0:50056', grpc.credentials.createInsecure());

const ProvidusService = grpc.load(providusPROTOPATH).payment;
const ProvidusRPC = new ProvidusService.Providus('0.0.0.0:50053', grpc.credentials.createInsecure());

const router = express.Router();


async function getBalance(req, res) {
  try {
    UserRPC.getUserWalletBalanceWithMobile({ mobile: req.query.mobile}, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
}

async function getBankAccountBeneficiaries(req, res) {
  try {
    UserRPC.getUserBankAccountBeneficiariesWithID({ id: req.eyowo_user._id }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      if (response.success) {
        response.beneficiaries = JSON.parse(response.beneficiaries);
      }
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
}

async function getTransactions(req, res) {
  try {
    UserRPC.getUserTransactionsWithID({ id: req.eyowo_user._id }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      if (response.success) {
        response.transactions = JSON.parse(response.transactions);
      }
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
}

// async function transfers(req, res) {
//   console.log(req.body);
//   if (req.query.type === 'mobile') {
//     const schema = Joi.object().keys({
//       amount: Joi.number().integer().required(),
//       recipient: Joi.string().regex(/^[0-9]{9,13}$/).required(),
//       description: Joi.string(),
//       secure_pin: Joi.string().regex(/[0-9]{6}$/).required(),
//     });

//     const validateResult = Joi.validate(req.body, schema);

//     if (validateResult.error !== null) {
//       res.json({ success: false, message: validateResult.error.details[0].message });
//       return;
//     }

//     try {
//       UserRPC.mobileFundsTransferForUserWithID({ id: req.eyowo_user._id }, (err, response) => {
//         if (err) {
//           throw err;
//         }
//         console.log(response);
//         if (response.success) {
//           response.transactions = JSON.parse(response.transactions);
//         }
//         res.json(response);
//       });
//     } catch (err) {
//       console.error(err);
//       res.status(500).send({ success: false, err });
//     }
//   } else if (req.query.type === 'bank') {
//     const schema = Joi.object().keys({
//       amount: Joi.number().integer().required(),
//       bank_code: Joi.string().required(),
//       account_number: Joi.string().required(),
//       description: Joi.string(),
//       secure_pin: Joi.string().regex(/[0-9]{6}$/).required(),
//     });

//     const validateResult = Joi.validate(req.body, schema);

//     if (validateResult.error !== null) {
//       res.json({ success: false, message: validateResult.error.details[0].message });
//     }
//     /*
//     try {
//       const response = await fundsTransferCtrl.processTransferBank(req.eyowo_user.mobile, { bank_code: req.body.bank_code, account_number: req.body.account_number }, req.body.amount, req.body.secure_pin);
//       console.log(response);
//       if (response.success) {
//         return res.json({ success: true, msg: response.message });
//       }
//       return res.status(response.status).send({ success: false, err: response.err });
//     } catch (err) {
//       console.error(err);
//       return res.status(500).send({ success: false, err });
//     }
//     */
//   } else {
//     res.status(400).send({ success: false, err: 'Type query required. Should be either mobile or bank.' });
//   }
// }

async function getBvnDetails(req, res) {
  const schema = Joi.object().keys({
    bvn: Joi.string().regex(/^[0-9]{11}$/).required(),
  });

  const validateResult = Joi.validate(req.body, schema);

  if (validateResult.error !== null) {
    res.json({ success: false, message: validateResult.error.details[0].message });
    return;
  }

  try {
    ProvidusRPC.bvn({ bvn: req.body.bvn }, (err, resp) => {
      if (err) throw err;
      res.json(resp);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
}


async function getUserDetails(req, res) {
  const schema = Joi.object().keys({
    mobile: Joi.string().regex(/^[0-9]{9,13}$/).required(),
  });

  const validateResult = Joi.validate(req.query, schema);

  if (validateResult.error !== null) {
    res.json({ success: false, message: validateResult.error.details[0].message });
    return;
  }

  try {
    UserRPC.getUserDetails({ mobile: req.query.mobile }, (err, resp) => {
      if (err) throw err;
      res.json(resp);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
}


async function transfers(req, res) {
  const schema = Joi.object().keys({
    sender: Joi.string().regex(/^[0-9]{9,13}$/).required(),
    amount: Joi.number().integer().required(),
    recipient: Joi.string().regex(/^[0-9]{9,13}$/).required(),
    description: Joi.string(),
    secure_pin: Joi.string().regex(/[0-9]{6}$/).required(),
  });

  const validateResult = Joi.validate(req.body, schema);

  if (validateResult.error !== null) {
    res.json({ success: false, message: validateResult.error.details[0].message });
    return;
  }

  console.log(req.body)
  try {
    UserRPC.creditUserWithMobile({
      sender: req.body.sender, amount: req.body.amount, recipient: req.body.recipient, remark: 'test', chargeCode: 'ABC100', pin: req.body.secure_pin
    }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
}

// ************************************************************************************************************************
//
//                  Authenticate all routes from here ON!!!!!
//
// ************************************************************************************************************************



router.get('/ussd/balance', getBalance);


// router.post('/:user_id/beneficiaries', [middlewareAuth.validateSession, bodyParser.json()], addBankAccountBeneficiary);
router.get('/:user_id/beneficiaries', middlewareAuth.validateSession, getBankAccountBeneficiaries);


router.get('/:user_id/transactions', middlewareAuth.validateSession, getTransactions);

router.get('/user_id/bvn', middlewareAuth.validateSession, getBvnDetails);

router.get('/ussd/userdetails', getUserDetails);

router.post('/ussd/transfers', [bodyParser.json()], transfers);
// router.post('/:user_id/transfers/multiple', [middlewareAuth.validateSession, bodyParser.json()], transfersMultiple);


module.exports = router;
